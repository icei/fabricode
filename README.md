# Required libraries

* Python3
* pygit2
* Roundup 1.5.1 (http://www.roundup-tracker.org/)
* jinja2
* cgit

# Installation (proposed)
1.  Do the following where Roundup tracker will be deployed:
    1.  Install Roundup 1.5.1 according to http://roundup-tracker.org/docs/installation.html#basic-installation-steps. Select the "jinja2" template, do not use any other templates.
    1.  Run the fabricode-tracker.patch file on the files in the tracker home directory.
    1.  Run roundup-admin initialise
    1.  Set cgiturl in extensions/config.ini to the URL to the project's cgit repo
    1.  Start Roundup
1.  Do the following on the git repository:
    1. Install cgit 1.1 and configure the path to the git repository
    1.  Place git-hook/post-receive in the git repo's "hook" directory
    1.  Make the script executable
    1.  Copy git-hook/fabricode.ini.sample to /etc/fabricode (create the directory as necessary)