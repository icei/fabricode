#!/usr/bin/env python3

from sys import exit
from os import getuid, getcwd
from xmlrpc.client import ServerProxy, Transport, Fault
from configparser import ConfigParser
import pygit2
import re
import logging

# Expected git commit keywords
_RELATEDTO_KEYWORD = 'relatedto'
_FIXES_KEYWORD = 'fixes'

class RoundupTransport(Transport):
    """
    Extend xmlrpc.client.Transport to provide X-REQUESTED-WITH header that the
    Roundup XML-RPC server expects
    """

    def send_content(self, connection, request_body):
        """
        Extend Transport.send_content() to add the header the XML-RPC server
        wants
        """

        connection.putheader('X-REQUESTED-WITH', 'xmlrpc')
        Transport.send_content(self, connection, request_body)

def create_gitcommit(server, cid, committer, shortlog):
    """
    Create the gitcommit class item
    """

    logger = logging.getLogger('create_gitcommit')
    logger.debug("Commit id: %s Committer: %s", cid, committer)

    gcid = server.create('gitcommit', 
        "committer=%s"%(committer),
        "shortlog=%s"%(shortlog),
        "commitid=%s"%(cid))
    logger.debug("GC ID: %s", gcid)

    return gcid

def process_commits(start_rev, end_rev, repo, server):
    """
    Process the newly pushed commits
    """

    logger = logging.getLogger('process_commits')

    logs = repo.walk(end_rev, pygit2.GIT_SORT_TOPOLOGICAL | pygit2.GIT_SORT_REVERSE)
    tagged_issues = {}

    # If not processing the first push to this repo, limit the logs to process
    # between start_rev and end_rev
    if start_rev != '00000':
        logger.debug('Not first commit on a new repo')
        logs.hide(start_rev)
    else:
        logger.debug('First commit on a new repo')

    # Walk through the log items
    parser = re.compile(r"\[(relatedto|fixes)\ (issue\d+)\]")
    for log in logs:
        cid = str(log.hex)[:5]
        logger.debug("Tree id: %s", cid)

        log_messages = log.message.splitlines()
        shortlog = log_messages[0]
        logger.debug("Value of shortlog: %s", shortlog)

        # gitcommit ID associated to commit being processed. Initialize to None
        # to indicate that a gitcommit item has not been created for the commit.
        gcid = None

        # Walk the commit log text and search for the keywords we expect
        for line in log_messages[1:]:
            logger.debug("Line: %s", line)
            match = parser.match(line)
            if match:
                action = match.group(1)
                target = match.group(2)

                if match.group(1) == _RELATEDTO_KEYWORD:
                    logger.info("Relate commit %s to %s", cid, target)
                elif match.group(1) == _FIXES_KEYWORD:
                    logger.info("Mark %s as fixed by %s", target, cid)

                # Create a new gitcommit item if needed
                if not gcid:
                    logger.debug('Create new gitcommit')
                    gcid = create_gitcommit(server, cid, log.author.name, shortlog)

                # Start an empty list if 1st time seeing target
                if target not in tagged_issues:
                    tagged_issues[target]={ _RELATEDTO_KEYWORD:[], _FIXES_KEYWORD:[] }
                tagged_issues[target][action].append(str(gcid))
            else:
                logger.debug("No match")

    logger.debug('Done processing log entries')

    return tagged_issues

def update_issues(tagged_issues, server):
    """
    Update the issues tagged in the commit logs that were pushed
    """

    logger = logging.getLogger('update_issues')

    # Get the status id for "testing" status
    test_id = server.lookup('status', 'testing')
    logger.debug("Testing status id: %s", test_id)

    for target in list(tagged_issues.keys()):
        logger.debug("Processing issue %s", target)
        gitcommits = ','.join(tagged_issues[target][_RELATEDTO_KEYWORD] + tagged_issues[target][_FIXES_KEYWORD])
        logger.debug("Associated commits: \"%s\"", gitcommits)

        try:
            if tagged_issues[target][_FIXES_KEYWORD]:
                logger.debug("Changing issue state to \"testing\"")
                server.set(target, "gitcommits= +%s"%(gitcommits), "status=%s"%(test_id))
            else:
                logger.debug("Only associating commits")
                server.set(target, "gitcommits= +%s"%(gitcommits))
        except Fault as e:
            fault_cause = e.faultString.split(':')[1]
            if not re.match("no such issue %s"%(target), fault_cause):
                logger.error("XML-RPC error encountered. Cause: %s", fault_cause)
                raise
            msg = "Failed to associate commits to %s. Cause: %s"%(target, e.faultString.split(':')[1])
            logger.info(msg)
            print(msg)

def main():
    logger = logging.getLogger()
    logging.getLogger('update_issues').setLevel(logging.DEBUG)
    try:
        config = ConfigParser()
        config.read('/etc/fabricode/fabricode.ini')
        url = config['githook']['url']
        logger.debug('Tracker URL: %s', url)

        server = ServerProxy(url, allow_none = 'True',
            transport = RoundupTransport())

        while True:
            s = input()
            logger.debug("Raw input: %s\n"%(s))

            [start_rev,end_rev,branch] = s.split(' ')
            start_rev = start_rev[:5]
            end_rev= end_rev[:5]
            logger.debug("Starting rev: %s Ending rev: %s Branch: %s",
                start_rev,end_rev,branch)

            if branch != 'refs/heads/master':
                logger.debug('Not the master branch. Moving on')
            else:
                logger.debug('Process commits to master branch')
                repo = pygit2.Repository(getcwd())
                tagged_issues = process_commits(start_rev, end_rev, repo, server)
                update_issues(tagged_issues, server)

                # Other branches that was pushed won't be processed; so, just
                # terminate processing from here
                return
    except EOFError:
        # The expectation is the script exits when stdin has been read
        logger.debug("Post-receive done")
    except BaseException as e:
       logger.error("Error encountered running post-receive hook: %s", e)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
    logging.shutdown()
